    $(document).ready(function() {
        $("#myImageCloud").imageCloud({
            width: 500,
            height: 500,
            color: '#aabb44',
            link: true,
            speed: 150,
            borderSize: 3,
            borderStyle: "groove",
            borderRadius: 3
        });
    });